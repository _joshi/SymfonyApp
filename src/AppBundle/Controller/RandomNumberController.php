<?php
/**
 * Created by PhpStorm.
 * User: sonu
 * Date: 16/07/16
 * Time: 12:39 AM
 */

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class RandomNumberController extends Controller
{
    /**
     * @Route("/random/number")
     */
    public function numberAction()
    {
        $number = rand(0, 100);

        return new Response(
            'Lucky number: '.$number
        );
    }

    /**
     * @Route("json/response")
     */
    public function jsonResponse()
    {
        $data = [
            'lucky_number' => rand(0, 100),
        ];

        return new Response(
            json_encode($data),
            200,
            array('Content-Type' => 'application/json')
        );
    }

    /**
     * @Route("json/response/class")
     */
    public function jsonResponseClass()
    {
        $data = [
            'lucky_number' => rand(0, 100),
        ];

        // calls json_encode() and sets the Content-Type header
        return new JsonResponse($data);
    }

    /**
     * @Route("dynamic/url/{number}")
     */
    public function dynamicUrl($number)
    {
        return new Response(
            '<i>Oh yeah!!!<br>Routing is sexy with Symfony xD</i><br>' .
            '<pre>Here\'s your fucking number: ' . $number . '</pre>'
        );
    }

    /**
     * YAML Routing sample
     */
    public function yamlRoutingAction($number)
    {
        return new Response(
            '<i>Wow!!! YAML routing is crazy af..<br>The Number in GET request is: </i>' . $number
        );
    }
    
    /**
     * @Route("twig/template/{number}")
     */
    public function twigTemplating($number)
    {
        $html = $this->container->get('templating')->render(
            'random/number.html.twig',
            ['number' => $number]
        );

        return new Response(
            $html
        );
    }

}